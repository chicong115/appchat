/*
    Event name: packet
    
    Response in JSON format:
    { sender: who, action: 'join' }
    { sender: who, action: 'broadcast', message: what }
    { sender: who, action: "list", userlist: list of users }
    { sender: who, action: "quit" }

*/
const { ClientServerMessage} = require('./mess');
const port = 3000;
const io = require('socket.io')(port);
const elasticsearch = require('@elastic/elasticsearch');
const es_client = new elasticsearch.Client({ node: 'http://localhost:9200' });


/* Load bcrypt module */    
const bcrypt = require('bcrypt');
const saltRounds = 10;



var user = []; 

console.log('Server is listening on port: %d', port);

/* Listen to the 'connect' event that fired upon a successfull connection */
io.on('connect', (socket) => {
    console.log("A user connected");

    /* Listen to 'disconnect' event that fired upon a successfull disconnection */
    socket.on('disconnect', (reason) => {
        console.log("A user disconnected, reason: %s", reason);

        /* Remove the user that disconnected */
        console.log(user);
        user.splice(user.findIndex(v => v.id === socket.id), 1);
        console.log("Number of users: %d", user.length);
    });

    /* Listen to 'packet' event from an individual socket */
    socket.on('packet', (packet_data) => {
        try{
            receivedPackage(packet_data, socket);
        }catch(error){
            console.error(error);
        };
        
    });
});

function broadcastAll(packet_data, socket){
    console.log(packet_data);
    socket.broadcast.emit('packet', packet_data);
}

function broadcastClient(packet_data, socket, es_client){
    socket.broadcast.emit('packet', packet_data);

                /* Save all messages to elasticsearch
                    index: room_messages
                    documents in the index:
                        { "group": socket.id, "sender": ted, "msg": "hi" }
                        { "group": socket.id, "sender": roni, "msg": "hi" }
                */
                /* Adds a JSON document to the specified index and makes it searchable.
                If the document already exists, updates the document and increments its version.*/
                es_client.create({
                    /* Document ID */
                    id: new Date().getTime(),

                    /* By default, the index is created automatically if it doesn’t exist */
                    index: "room_messages",

                    /* If true then refresh the affected shards to make this operation visible
                    to search */
                    refresh: 'true',

                    /* Document data */
                    body: {
                        "group": socket.id,
                        "sender": packet_data.sender,
                        "msg": packet_data.msg
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (201 === result.statusCode) {
                        console.log("Saved the message to the database");
                    }
                });
}

function join(packet_data,socket){
    let newuser = {
        name : packet_data.sender,
        id : socket.id
    };
    user.push(newuser);

    console.log("Nickname: ", packet_data.sender, ", ID: ", socket.id);
    console.log("Number of users: %d", user.length);
    /* Save nickname to this socket object */
    socket.nickname = packet_data.sender;

    /*  Broadcasting means sending a packet to everyone else
    except for the socket that starts it */
    socket.broadcast.emit('packet', packet_data);
}

function joinSecure(packet_data,socket){
    let newuser = {
        name : packet_data.sender,
        id : socket.id
    };
    user.push(newuser);
    console.log("Nickname: ", packet_data.sender, ", ID: ", socket.id);
    console.log("Number of users: %d", user.length);

        es_client.search({
        index: "accounts",

        /* Search queries */
        body: {
               "query": {
                "match": {
                "nickname": packet_data.sender
                    }
                    }
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    console.log(result);
                    if (404 === result.statusCode ||
                        /* Index not found, this means this is the first time that a user join,
                            so just save the account information */

                        (200 === result.statusCode && null === result.body.hits.max_score)) {
                        /* Nickname not found, this means this is the first time that a user join,
                            so just save the account information */

                        if (404 === result.statusCode) { console.log("Index not found!"); }
                        else { console.log("Nickname not found!"); }

                        bcrypt.hash(packet_data.password, saltRounds, function(err, hashed_password) {
                            console.log(err);

                            console.log(packet_data.password);
                            /* Adds a JSON document to the specified index and makes it searchable.
                            If the document already exists, updates the document and increments its version.*/
                            es_client.create({
                                /* Document ID */
                                id: new Date().getTime(),

                                /* By default, the index is created automatically if it doesn’t exist */
                                index: "accounts",

                                /* If true then refresh the affected shards to make this operation visible
                                to search */
                                refresh: 'true',

                                /* Document data */
                                body: {
                                    "nickname": packet_data.sender,
                                    "password": hashed_password
                                }
                            }, (err, result) => {
                                if (err) console.log(err);
                                if (201 === result.statusCode) {
                                    console.log("Saved the account to the database");
                                }
                            });
                        });

                    } else {
                        /* Found the nickname, check if the password matches or not */
                        try{
                        console.log(packet_data.password);
                        console.log(result.body.hits.hits[0]._source.password);

                        bcrypt.compare(packet_data.password, result.body.hits.hits[0]._source.password,
                                        function(err, result) {
                            if (true === result) {
                                console.log("Correct password");

                                /*  Broadcasting means sending a packet to everyone else
                                except for the socket that starts it */
                                socket.broadcast.emit('packet', packet_data);
                            } else {
                                console.log("Incorrect password!");
                                socket.disconnect(true);
                            }
                        });
                    }catch(error){
                        console.log(error);
                    }
                    }
                });

                /* Save nickname to this socket object */
                socket.nickname = packet_data.sender;
}

function receivedPackage (packet_data, socket){
    switch (packet_data.action) {
        case ClientServerMessage.join, ClientServerMessage.connection:
            joinSecure(packet_data,socket);
        break;

        case ClientServerMessage.broadcast:
            //broadcastAll(packet_data, socket);
            broadcastClient(packet_data, socket);
        break;

        case ClientServerMessage.list:
            var users = [];
            /* Search all nicknames */
            for (var key in io.of('/').sockets) {
                let usrName = io.of('/').sockets[key].nickname;
                if(usrName){
                    users.push(usrName);
                }
            }
            /* Sending a packet to socket that starts it */
            socket.emit('packet', {"sender": packet_data.sender,
                                    "action": "list",
                                    "users": users});

        break;

        case ClientServerMessage.quit:
            /*  Broadcasting means sending a packet to everyone else
            except for the socket that starts it */
            socket.broadcast.emit('packet', packet_data);
            socket.disconnect(true);
        break;



        case ClientServerMessage.send:
            var socket_id = null;

            /* Search all nicknames */
            for (var key in io.of('/').sockets) {
                console.log(packet_data.dest);
                console.log(io.of('/').sockets[key].nickname);
                if (packet_data.dest === io.of('/').sockets[key].nickname) {
                    socket_id = io.of('/').sockets[key].id;
                }
            }


            if (socket_id !== null) {
                /* Each socket is identified by a random,
                unguessable, unique identifier Socket#id. For your convenience,
                each socket automatically joins a room identified by its own id. */
                io.to(socket_id).emit('packet', packet_data);

                /* Save all messages to elasticsearch
                index: room_messages
                documents in the index:
                    { "group": socket.id, "sender": ted, "msg": "hi" }
                    { "group": socket.id, "sender": roni, "msg": "hi" }
                */
                /* Adds a JSON document to the specified index and makes it searchable.
                If the document already exists, updates the document and increments its version.*/
                es_client.create({
                    /* Document ID */
                    id: new Date().getTime(),

                    /* By default, the index is created automatically if it doesn’t exist */
                    index: "room_messages",

                    /* If true then refresh the affected shards to make this operation visible
                    to search */
                    refresh: 'true',

                    /* Document data */
                    body: {
                        "group": socket.id,
                        "sender": packet_data.sender,
                        "msg": packet_data.msg
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (201 === result.statusCode) {
                        console.log("Saved the message to the database");
                    }
                });
            }else{
                console.log("send to wrong user");
            }
        break;

        /* Create a group: cg;friends */
        case ClientServerMessage.cgroup:
            console.log("need?");
        break;

        /* Join a group: j;friends */
        case ClientServerMessage.jgroup:
            socket.join(packet_data.group, () => {
                console.log("Group: ", packet_data.group, ", Joined: ", packet_data.sender);

                /* Sending to all clients in a group, including sender */
                io.to(packet_data.group).emit('packet', packet_data);
            });
        break;

        /* Broadcast a message to a group: bg;friends;hello */
        case ClientServerMessage.gbroadcast:
            /* Sending to all clients in a group except sender */
            socket.to(packet_data.group).emit('packet', packet_data);

            /* Save all messages belonging a group to elasticsearch
                index: room_messages
                documents in the index:
                    { "group": friends, "sender": ted, "msg": "hi" }
                    { "group": friends, "sender": roni, "msg": "hi" }
            */
            /* Adds a JSON document to the specified index and makes it searchable.
            If the document already exists, updates the document and increments its version.*/
            es_client.create({
                /* Document ID */
                id: new Date().getTime(),

                /* By default, the index is created automatically if it doesn’t exist */
                index: "room_messages",

                /* If true then refresh the affected shards to make this operation visible
                to search */
                refresh: 'true',

                /* Document data */
                body: {
                    "group": packet_data.group,
                    "sender": packet_data.sender,
                    "msg": packet_data.msg
                }
            }, (err, result) => {
                if (err) console.log(err);
                if (201 === result.statusCode) {
                    console.log("Saved the message to the database");
                }
            });
        break;

        /* List all clients that are inside a group: members;friends */
        case ClientServerMessage.members:
            var members = [];

            io.of('/').in(packet_data.group).clients((error, clients) => {
                if (error) throw error;

                /* clients are an array of socket ids in this group */
                for (var i = clients.length - 1; i >= 0; i--) {
                    members.push(io.of('/').sockets[clients[i]].nickname);
                }

                /* Sending a packet to socket that starts it */
                socket.emit('packet', {"sender": packet_data.sender,
                                        "action": "members",
                                        "group": packet_data.group,
                                        "members": members});
            });
        break;

        /* List the history of messages exchanged in a group:
        messages;friends */
        case ClientServerMessage.msgs:
            var msgs = [];
            /*  index: room_messages
                documents in the index:
                    { "group": friends, "sender": ted, "msg": "hi" }
                    { "group": friends, "sender": roni, "msg": "hi" }
            */
            es_client.search({
                index: "room_messages",

                /* Search queries */
                body: {
                    "query": {
                        "match": {
                            "group": packet_data.group
                        }
                    }
                }
            }, (err, result) => {
                if (err) console.log(err);

                for (var i = 0; i < result.body.hits.hits.length; i++) {
                    msgs.push(result.body.hits.hits[i]._source.sender + ": " +
                        result.body.hits.hits[i]._source.msg);
                }

                /* Sending a packet to socket that starts it */
                socket.emit('packet', {"sender": packet_data.sender,
                                        "action": "msgs",
                                        "group": packet_data.group,
                                        "msgs": msgs});
            });
        break;

        /* List the history of messages belonging to a user:
        umessages;ted */
        case ClientServerMessage.umsgs:
            var msgs = [];
            /*  index: room_messages
                documents in the index:
                    { "group": friends, "sender": ted, "msg": "hi" }
                    { "group": friends, "sender": roni, "msg": "hi" }
            */
            es_client.search({
                index: "room_messages",

                /* Search queries */
                body: {
                    "query": {
                        "match": {
                            "sender": packet_data.user
                        }
                    }
                }
            }, (err, result) => {
                if (err) console.log(err);

                for (var i = 0; i < result.body.hits.hits.length; i++) {
                    msgs.push(result.body.hits.hits[i]._source.group + ": " +
                        result.body.hits.hits[i]._source.msg);
                }

                /* Sending a packet to socket that starts it */
                socket.emit('packet', {"sender": packet_data.sender,
                                        "action": "umsgs",
                                        "user": packet_data.user,
                                        "msgs": msgs});
            });
        break;

        /* List the existing groups: groups; */
        case ClientServerMessage.groups:
            const rooms = io.of('/').adapter.rooms;

            /* Sending a packet to socket that starts it */
            socket.emit('packet', {"sender": packet_data.sender,
                                    "action": "groups",
                                    "groups": Object.keys(rooms)});

        break;

        /* Leave a group: leave;friends */
        case ClientServerMessage.leave:
            socket.leave(packet_data.group, () => {
                console.log("Group: ", packet_data.group, ", Left: ", packet_data.sender);

                /* Sending to all clients in a group, including sender */
                io.to(packet_data.group).emit('packet', packet_data);
            });
        break;




        case ClientServerMessage.secure_1:
            var socket_id = null;

            console.log("[%s]: Public key: %s", packet_data.sender,
                                                packet_data.public_key.toString('hex'));

            /* Search all nicknames */
            for (var key in io.of('/').sockets) {
                if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
                    socket_id = io.of('/').sockets[key].id;
                }
            }

            if (socket_id !== null) {
                /* Each socket is identified by a random,
                unguessable, unique identifier Socket#id. For your convenience,
                each socket automatically joins a room identified by its own id. */
                io.to(socket_id).emit('packet', packet_data);
            }
        break;

        case ClientServerMessage.secure_2:
            var socket_id = null;

            console.log("[%s]: Public key: %s", packet_data.sender,
                                                packet_data.public_key.toString('hex'));

            /* Search all nicknames */
            for (var key in io.of('/').sockets) {
                if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
                    socket_id = io.of('/').sockets[key].id;
                }
            }

            if (socket_id !== null) {
                /* Each socket is identified by a random,
                unguessable, unique identifier Socket#id. For your convenience,
                each socket automatically joins a room identified by its own id. */
                io.to(socket_id).emit('packet', packet_data);
            }
        break;

        case ClientServerMessage.secure_send:
            var socket_id = null;

            console.log("[%s]: Encrypted message: %s\n\tIV: %s", packet_data.sender,
                                                    packet_data.encrypted_msg,
                                                    packet_data.iv.toString('hex'));

            /* Search all nicknames */
            for (var key in io.of('/').sockets) {
                if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
                    socket_id = io.of('/').sockets[key].id;
                }
            }

            if (socket_id !== null) {
                /* Each socket is identified by a random,
                unguessable, unique identifier Socket#id. For your convenience,
                each socket automatically joins a room identified by its own id. */
                io.to(socket_id).emit('packet', packet_data);
            }
        break;




        /* For debugging: debug */
        case ClientServerMessage.debug:
            console.log(io.of('/'));
        break;


        default:
        break;
    }
}
