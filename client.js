/*
    Event name: packet
    
    Request in JSON format:
    { sender: who, action: "join" }
    { sender: who, action: "broadcast", message: what }
    { sender: who, action: "list" }
    { sender: who, action: "quit" }
*/
const { ClientServerMessage, InputFormat} = require('./mess');

const io = require('socket.io-client');
const socket = io.connect('http://localhost:3000');
const readline = require('readline');
const crypto = require('crypto'); /* Load built-in 'crypto' module */

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var nickname = null;
var password = null;

var patternPrivate = /^s;([A-Z0-9]+);(.+)/i;
var patternGroup = /^bg;([A-Z0-9]+);(.+)/i; // for group broadcast
var patternSecure = /^sss;([A-Z0-9]+);(.+)/i; // for sending an encrypted message

var ecdh;
var my_public_key, partner_public_key, secret_key;



/* Listen to the 'connect' event that fired upon a successfull connection */
socket.on('connect', () => {
    nickname = process.argv[2];
    password = process.argv[3];

    if(!nickname){
        nickname = new Date().getTime().toString();
        console.log("[INFO]: Empty Name, Set name at %s", nickname);
    }
    if(!password){
        password = new Date().getTime().toString();
        console.log("[INFO]: Empty Password, Set password of %s is %s", nickname, password);
    }
    console.log("[INFO]: Welcome %s", nickname);
   // socket.emit('packet', {"sender": nickname, "action": ClientServerMessage.join});
    socket.emit('packet', {"sender": nickname, "action": ClientServerMessage.connection, "password": password});
});

/* Listen to the 'disconnect' event that fired upon a successfull disconnection */
socket.on('disconnect', (reason) => {
    console.log("[INFO]: Server disconnected, reason: %s", reason);
    rl.close();
});


/*
Convert input to command
*/


function Command(mess){
    if(mess.startsWith(InputFormat.join)){
        return ClientServerMessage.join;
    }
    if(mess.startsWith(InputFormat.connection)){
        return ClientServerMessage.connection;
    }
    if(mess.startsWith(InputFormat.broadcast)){
        return ClientServerMessage.broadcast;
    }
    if(mess.startsWith(InputFormat.list)){
        return ClientServerMessage.list;
    }
    if(mess.startsWith(InputFormat.quit)){
        return ClientServerMessage.quit;
    }


    if(patternPrivate.test(mess)){
        return ClientServerMessage.send;
    }
    //if(mess.startsWith(InputFormat.send)){
    //    return ClientServerMessage.send;
    //}
    if(mess.startsWith(InputFormat.cgroup)){
        return ClientServerMessage.cgroup;
    }
    if(mess.startsWith(InputFormat.jgroup)){
        return ClientServerMessage.jgroup;
    }

    if(patternGroup.test(mess)){
        return ClientServerMessage.gbroadcast;
    }

    if(mess.startsWith(InputFormat.secure_1)){
        return ClientServerMessage.secure_1;
    }
    if(mess.startsWith(InputFormat.secure_2)){
        return ClientServerMessage.secure_2;
    }
    if(patternSecure.test(mess)){
        return ClientServerMessage.secure_send;
    }

    if(mess.startsWith(InputFormat.ss)){
        return ClientServerMessage.ss;
    }



    if(mess.startsWith(InputFormat.debug)){
        return ClientServerMessage.debug;
    }
    return ClientServerMessage.unavai;
}

/* 
client enter and then to server
 */
rl.on('line', (input) => {
    let commandAction = Command(input);
    switch(commandAction){
        case ClientServerMessage.broadcast :
            var str = input.slice(2);
            socket.emit('packet', {"sender": nickname, "action": commandAction,
                                    "msg": str});    
            break;
        case ClientServerMessage.list :
            socket.emit('packet', {"sender": nickname, "action": commandAction});
                break;
        case ClientServerMessage.quit :
            socket.emit('packet', {"sender": nickname, "action": commandAction});
            break;
            case ClientServerMessage.debug :
                socket.emit('packet', {"sender": nickname, "action": commandAction});
                break; 
        case ClientServerMessage.send:
            var info = input.match(patternPrivate);
            console.log(info);
            socket.emit('packet', {"sender": nickname, "action": commandAction,
                                    "dest": info[1], "msg": info[2]});    
                break;
        case ClientServerMessage.cgroup:
            var str = input.slice(3);
            socket.emit('packet', {"sender": nickname, "action": commandAction,
                                    "group": str});    
            break;
        case ClientServerMessage.jgroup:
            var str = input.slice(2);
            socket.emit('packet', {"sender": nickname, "action": commandAction,
                                    "group": str});
            break;
        case ClientServerMessage.gbroadcast:
            var info = input.match(patternGroup);
            socket.emit('packet', {"sender": nickname, "action": commandAction,
                                    "group": info[1], "msg": info[2]});
            break;
         case ClientServerMessage.members:
            var str = input.slice(8);
            socket.emit('packet', {"sender": nickname, "action": commandAction,
                                    "group": str});
            break;                                    
        case ClientServerMessage.msgs:
                var str = input.slice(9);
                socket.emit('packet', {"sender": nickname, "action": commandAction,
                                        "group": str});
                break;   
        case ClientServerMessage.umsgs:
                var str = input.slice(10);
                socket.emit('packet', {"sender": nickname, "action": commandAction,
                "user": str});
                break;   
        case ClientServerMessage.groups:
            socket.emit('packet', {"sender": nickname, "action": commandAction});
                    break;   
        case ClientServerMessage.leave:
            var str = input.slice(6);
            socket.emit('packet', {"sender": nickname, "action": commandAction,
                                    "group": str});
            break;  
        case ClientServerMessage.ss:
            var str = input.slice(3);
            console.log(str);
            // Create an ecdh instance with key size of 256 bits
            ecdh = crypto.createECDH('secp256k1');
    
            // Generates private and public key, and returns the public key
            my_public_key = ecdh.generateKeys();
    
            socket.emit('packet', {"sender": nickname, "action": "secure_1",
                                    "dest": str, "public_key": my_public_key});
            break;
            case ClientServerMessage.secure_1:
                var str = input.slice(3);
                console.log(str);
                // Create an ecdh instance with key size of 256 bits
                ecdh = crypto.createECDH('secp256k1');
        
                // Generates private and public key, and returns the public key
                my_public_key = ecdh.generateKeys();
        
                socket.emit('packet', {"sender": nickname, "action": commandAction,
                                        "dest": str, "public_key": my_public_key});
                                        break;
        case ClientServerMessage.secure_2:
                var str = input.slice(3);
        
                // Create an ecdh instance with key size of 256 bits
                ecdh = crypto.createECDH('secp256k1');
        
                // Generates private and public key, and returns the public key
                my_public_key = ecdh.generateKeys();
        
                socket.emit('packet', {"sender": nickname, "action": commandAction,
                                        "dest": str, "public_key": my_public_key});
        break;

        
            case ClientServerMessage.secure_send:
            /* Send an encrypted message to Ted: sss;ted;hello ted */
                var info = input.match(patternSecure);
        
                // Length of secret key is dependent on the algorithm. In this case for aes256,
                // it requires 32 bytes (256 bits), please pay attention that our secret key
                // generated in the establish steps meet this requirement
                const algorithm = 'aes-256-cbc';
        
                // Initialization vector
                // Block sizes of AES are 128 bits, iv has the same size as the block size
                const randome_iv = crypto.randomBytes(16);
        
                // Create cipher object with the given arguments
                const cipher = crypto.createCipheriv(algorithm, secret_key, randome_iv);
        
                // Start to encrypt
                let encrypted = cipher.update(info[2] /* raw message */, 'utf8', 'hex');
                encrypted += cipher.final('hex');
        
                // Send iv along with the encrypted message for decrypting message
                socket.emit('packet', {"sender": nickname, "action": commandAction,
                                        "dest": info[1], "encrypted_msg": encrypted, "iv": randome_iv});
            break;
        case ClientServerMessage.unavai:
                    console.log("[ERROR]: don't support this kind of syntax");
                    break;    
        default :
            console.log("[ERROR]: don't support this kind of syntax");
            break;
    };
});

/* Listen to the 'packet' event that is emitted from the server */
socket.on('packet', (packet_data) => {
    try{
    receivedPackage(packet_data);
    }catch(error){
        console.error(error);
    }
});


function receivedPackage(packet_data){
    switch (packet_data.action) {
        case ClientServerMessage.join:
            console.log("[INFO]: %s has joined the chat", packet_data.sender);
        break;
        case ClientServerMessage.connection:
            console.log("[INFO]: %s has joined the chat", packet_data.sender);
        break;
        case ClientServerMessage.broadcast:
            console.log("%s : %s", packet_data.sender, packet_data.msg);
        break;

        case ClientServerMessage.list:
            console.log(packet_data);
            console.log("[INFO]: List of nicknames:");
            for (var i = packet_data.users.length - 1; i >= 0; i--) {
                console.log(packet_data.users[i]);
            }
        break;


        case ClientServerMessage.send:
            console.log("%s", packet_data.msg);
        break;

        case ClientServerMessage.cgroup:
            // console.log("need?");
        break;

        case ClientServerMessage.jgroup:
            console.log("[INFO]: %s has joined the group", packet_data.sender);
        break;

        case ClientServerMessage.gbroadcast:
            console.log("%s", packet_data.msg);
        break;

        case ClientServerMessage.members:
            console.log("[INFO]: List of members:");
            for (var i = 0; i < packet_data.members.length; i++) {
                console.log(packet_data.members[i]);
            }
        break;

        case ClientServerMessage.msgs:
            console.log("[INFO]: History of messages in group", packet_data.group);
            for (var i = 0; i < packet_data.msgs.length; i++) {
                console.log(packet_data.msgs[i]);
            }
        break;

        case ClientServerMessage.umsgs:
            console.log("[INFO]: History of messages belong to", packet_data.user);
            for (var i = 0; i < packet_data.msgs.length; i++) {
                console.log(packet_data.msgs[i]);
            }
        break;

        case ClientServerMessage.groups:
            console.log("[INFO]: List of groups:");
            for (var i = 0; i < packet_data.groups.length; i++) {
                console.log(packet_data.groups[i]);
            }
        break;

        case ClientServerMessage.leave:
            console.log("[INFO]: %s left the group", packet_data.sender);
        break;


        case ClientServerMessage.quit:
            console.log("[INFO]: %s quit the chat", packet_data.sender);
        break;

        case ClientServerMessage.secure_1:
            partner_public_key = packet_data.public_key;

            // Create an ecdh instance with key size of 256 bits
            ecdh = crypto.createECDH('secp256k1');

            // Generates private and public key, and returns the public key
            my_public_key = ecdh.generateKeys();

            // Send my public key back to the sender
            socket.emit('packet', {"sender": nickname, "action": "secure_2",
                                    "dest": packet_data.sender, "public_key": my_public_key});
            // Generates the secret key, please pay attention that this key is the same at two ends
            secret_key = ecdh.computeSecret(partner_public_key);

            // For debugging
            // console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
        break;

        case ClientServerMessage.secure_2:
            partner_public_key = packet_data.public_key;

            // Generates the secret key, please pay attention that this key is the same at two ends
            secret_key = ecdh.computeSecret(partner_public_key);
            
            // For debugging
            // console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
        break;

        case ClientServerMessage.secure_send:
            // Use the same algorithm as the sender
            const algorithm = 'aes-256-cbc';

            // Initialization vector from the sender
            const iv = packet_data.iv;

            // Create decipher object with the given arguments
            const decipher = crypto.createDecipheriv(algorithm, secret_key, iv);

            // Start to decrypt the message from the sender
            let decrypted = decipher.update(packet_data.encrypted_msg, 'hex', 'utf8');
            decrypted += decipher.final('utf8');

            // Show the decrypted message at our end
            console.log("%s", decrypted);
        break;


        default:
            console.log("[ERROR]: don't support this kind of syntax");
        break;
    }
}