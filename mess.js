'use strict';


const ClientServerMessage = {
    join : "join",
    broadcast : "broadcast",
    list : "list",
    quit : "quit",

/* TD2*/
    send: "send",
    cgroup: "cgroup",
    jgroup: "jgroup",
    gbroadcast: "gbroadcast",
    members: "members",
    msgs: "msgs",
    umsgs: "umsgs",
    groups: "groups",
    leave: "leave",

    secure_1: "secure_1",
    ss: "ss;",

    secure_2: "secure_2",
    secure_send: "secure_send",



    debug : "debug",
    unavai : ""


};

const InputFormat = {
    join : "join;",
    broadcast : "b;",
    list : "ls;",
    quit : "q;",

    send: "s;",
    cgroup: "cg;",
    jgroup: "j;",
    gbroadcast: "gbroadcast",
    members: "members;",
    msgs: "messages;",
    umsgs: "umessages;",
    groups: "groups;",
    leave: "leave;",
    
    ss: "ss;",
    secure_1: "secure_1",
    secure_2: "secure_2",
    secure_send: "ss",


    debug : "d;",
    unavai : ""

};
module.exports = {
    ClientServerMessage, InputFormat
}
